// execute after document ready
jQuery(document).ready(function(){
    // Client logos slider
	jQuery('.clients-logos').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 5000,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        dots: false,
        pauseOnHover: false
    });

    // testimonial slider
    jQuery('.testimonial-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        speed: 1200,
        autoplay: false,
        autoplaySpeed: 2000,
    });

    // blogs slider
    if(jQuery(window).width() < 769){
        jQuery('.blogs-row').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            speed: 1200,
            autoplay: true,
            autoplaySpeed: 2000,
        });

        jQuery('.leader-row, .hiring-row').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            speed: 1200,
            autoplay: true,
            autoplaySpeed: 2000,
        });
    }

    // lazy load on page load
    jQuery('.animation-wrap').each(function() {
        if(jQuery(this).offset().top < jQuery(window).height()){
            jQuery(this).addClass('in-view');
        }
    });

    // lazy load function
    function isScrolledIntoView(element) {
        var windowTop = jQuery(window).scrollTop();
        var windowBottom = windowTop + jQuery(window).height();
        var elementTop = jQuery(element).offset().top;
        var elementBottom = elementTop + jQuery(element).outerHeight();

        if ((elementBottom >= windowTop) && (elementTop <= windowBottom)) {
            jQuery(element).addClass('in-view');
        }
    }

    // lazy load on scroll
    jQuery(window).scroll(function () {
        setTimeout(function(){
            jQuery('.animation-wrap').each(function () {
                isScrolledIntoView(this);
            })
       },250);
    });

    // fixed header on scroll
    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = jQuery('.header-wrap').outerHeight();
    var headerHeight = jQuery('.image-header').outerHeight() + 120;

    jQuery(window).scroll(function(event){
        didScroll = true;
    });
    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);
    function hasScrolled() {
        var st = jQuery(this).scrollTop();
        if(Math.abs(lastScrollTop - st) <= delta)
        return;
        if (st > lastScrollTop && st > navbarHeight){
            jQuery('.header-wrap').removeClass('nav-down').addClass('nav-up');
        } else {
            if(st + jQuery(window).height() < jQuery(document).height()) {
                if(jQuery('.image-header').length){
                    if(lastScrollTop >= headerHeight){
                        jQuery('.header-wrap').removeClass('transparent-nav nav-up').addClass('nav-down');
                    }else{
                        jQuery('.header-wrap').removeClass('nav-up').addClass('transparent-nav nav-down');
                    }
                }else{
                    jQuery('.header-wrap').removeClass('nav-up').addClass('nav-down');
                }                
            }
        }

        lastScrollTop = st;
    }

    // show hide navigation on click of button for mobile devices
    jQuery('.btn-menu').click(function(){
        jQuery('.navigation-wrap').addClass('active');
    });

    // hide navifation for mobile devices
    jQuery('.btn-close').click(function(){
        jQuery('.navigation-wrap').removeClass('active');
    });

    // scroll down to contact form on click of link
    jQuery("#contact-link").click(function() {
        jQuery('html, body').animate({
            scrollTop: jQuery("#contact-footer").offset().top
        }, 800);
    });

    // leaders section
    jQuery('.triangle-shape').click(function(){
        var curIndex = jQuery(this).attr('data-index');
        if(curIndex){
			jQuery('.triangle-shape').each(function(index, item){
                if(jQuery(item).attr('data-index')){
                    jQuery(item).children().attr('filter','url(#grayscale)');
                }
			});
            jQuery(this).children().removeAttr('filter');
            jQuery('.leader-content-wrap .leader-content').removeClass('current');
            jQuery('.leader-content-wrap .leader-content-'+curIndex).addClass('current');
        }

    });
  
    // leader functionality on hover only for desktop devices
    if(jQuery(window).width() > 768){
        jQuery('.triangle-shape').hover(function(){
            var curIndex = jQuery(this).attr('data-index');
            if(curIndex){
                jQuery('.triangle-shape').each(function(index, item){
                    if(jQuery(item).attr('data-index')){
                        jQuery(item).children().attr('filter','url(#grayscale)');
                    }
                });
                jQuery(this).children().removeAttr('filter');
                jQuery('.leader-content-wrap .leader-content').removeClass('current');
                jQuery('.leader-content-wrap .leader-content-'+curIndex).addClass('current');
            }

        });
    }

});

jQuery(window).load(function(){
    // show more text
    var wrapperHeight, contentHeight;
    jQuery('.testimonial-right .testimonial-content').each(function(){
        wrapperHeight = jQuery(this).height();
        contentHeight = jQuery(this).find('.testimonial-inner').outerHeight();
        if(contentHeight > wrapperHeight){
            jQuery(this).next('.more-link').show();
        }
    });
    jQuery('.more-link').click(function(){
        if(jQuery(this).hasClass('hide-text')){
            jQuery(this).prev('.testimonial-content').find('.mCSB_container').css('height', '100%');
            jQuery(this).html('read more').removeClass('hide-text');
        }else{
            jQuery(this).prev('.testimonial-content').find('.mCSB_container').css('height', 'auto');
            jQuery(this).html('read less').addClass('hide-text');
        }
    });

    // custom scrollbar for testimonial more content
    jQuery('.testimonial-content').mCustomScrollbar({ 
        theme:"dark-3"        
    });

    // hide navigation bar on side click for mobile devices
   jQuery(document).click(function(e){
        e.stopPropagation();
        if(!jQuery(e.target).hasClass('btn-menu') && !jQuery(e.target).parents('.btn-menu').length && !jQuery(e.target).hasClass('navigation-wrap') && !jQuery(e.target).parents('.navigation-wrap').length){
            jQuery('.navigation-wrap').removeClass('active');
        }
    });

   // custom file attachment
    jQuery(".attach-field input[type='file']").change(function () {
        var fileName = this.files[0].name;
        jQuery(this).next().text(fileName).addClass('active');
    });

});
